//FirstView Component Constructor
function FirstView() {
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView({
	    layout : 'vertical'
	});
	
	self.add(Ti.UI.createLabel({
        color:'#000000',
        text : 'Benchmark Telas',
        left : '5dp', 
        right : '5dp', 
        top : '5dp', 
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    }));
    
    var view1 = Ti.UI.createView({
       layout : 'horizontal', 
    });
    
    var button1 = Ti.UI.createButton({
        title : 'Teste 1', 
        width : '70%'
    });
    
    var infoButton1 = Ti.UI.createButton({
        title : 'Info', 
        width : '30%'
    });
    
    view1.add(button1);
    view1.add(infoButton1);
    
    self.add(view1);

	return self;
}

module.exports = FirstView;
